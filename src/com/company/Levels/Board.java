package com.company.Levels;

import com.company.Typedefs.*;

import java.util.ArrayList;

public class Board {
    private ArrayList<Board_Node> board_map = new ArrayList<Board_Node>();
    private ArrayList<Piece> piece_list = new ArrayList<Piece>();

    public boolean addNode(Integer id) {
        Board_Node found_node = findNodeInMap(id);

        //check if node already present
        if (found_node == null) {
            Board_Node new_node = new Board_Node(id);
            board_map.add(new_node);
            return true;
        }
        else
            return false;
    }

    public boolean removeNode(Integer id) {
        Board_Node found_node = findNodeInMap(id);

        if (found_node != null) {
            board_map.remove(found_node);
            return true;
        }
        else return false;
    }

    private Board_Node findNodeInMap(Integer id_node) {
        for (Board_Node current_node : board_map) {
            if (current_node.getId().equals(id_node))
                return current_node;
        }

        return null;
    }

    Boolean addPiece(Integer id_player, Integer id_piece, Integer id_node) {
        Piece found_piece = findPiece(id_player, id_piece);

        if (found_piece != null || !isNodeFree(id_node))
            return false;
        else {
            Piece new_piece = new Piece(id_player, id_piece, id_node);
            piece_list.add(new_piece);
            return true;
        }
    }

    Piece getPieceInNode(Integer id_node) {
        for (Piece current_piece : piece_list) {
            if (current_piece.getBoardPosition().equals(id_node))
                return current_piece;
        }

        return null;
    }

    private Piece findPiece(Integer id_player, Integer id_piece) {
        for (Piece piece : piece_list) {
            if (piece.equalsOwnerAndId(id_player, id_piece))
                return piece;
        }

        return null;
    }

    public boolean setPiecePosition(Integer id_player, Integer id_piece, Integer target_node) {
        Piece target_piece = findPiece(id_player, id_piece);

        if (target_piece != null) {
            target_piece.setBoardPosition(target_node);
            return true;
        }

        return false;
    }

    public boolean removePiece(Integer id_player, Integer id_piece) {
        Piece target_piece = findPiece(id_player, id_piece);

        if (target_piece != null) {
            piece_list.remove(target_piece);
            return true;
        }

        return false;
    }

    public ArrayList<Piece> getAllPiecesOfPlayer(Integer id_player) {
        ArrayList<Piece> player_pieces = new ArrayList<Piece>();

        for (Piece current_piece : piece_list) {
            if (current_piece.getPlayer().equals(id_player)) {
                player_pieces.add(current_piece);
            }
        }

        return player_pieces;
    }

    boolean isNeighbor(Integer id_source, Integer id_target) {
        Board_Node source_node = findNodeInMap(id_source);

        if (source_node == null)
            return false;
        else return source_node.hasNeighbor(id_target);
    }

    public boolean isNodeFree(Integer id_node) {
        for (Piece piece : piece_list) {
            if (piece.getBoardPosition().equals(id_node))
                return false;
        }

        return true;
    }

    Integer getNumberOfPiecesOnTheWay(Integer first_node_index, Integer second_node_index) {
        Board_Node first_node = findNodeInMap(first_node_index);
        Board_Node second_node = findNodeInMap(second_node_index);

        if (first_node != null && second_node != null)
            return getNumberOfPiecesOnTheWay(first_node, second_node);
        else return -1;
    }

    Integer getNumberOfPiecesOnTheWay(Board_Node first_node, Board_Node second_node) {
        Direction aligned_direction = getAlignedDirection(first_node, second_node);
        boolean not_aligned = (aligned_direction == Direction.NULL);

        if (not_aligned)
            return -1;
        else {
            Integer first_neighbor = first_node.getIndexOfNeighborInDirection(aligned_direction);

            ArrayList<Integer> aligned_nodes = getAlignedNodesInDirection(first_neighbor, aligned_direction, new ArrayList<Integer>());
            Integer piece_count = 0;

            //Refactor
            for (Integer node_index : aligned_nodes) {
                if (node_index.equals(second_node.getId()))
                    return piece_count;
                else if (getPieceInNode(node_index) != null)
                    piece_count++;
            }

            return piece_count;
        }
    }

    boolean isAligned(Integer first_node_index, Integer second_node_index) {
        return (getAlignedDirection(first_node_index, second_node_index) != null);
    }

    boolean isAligned(Board_Node first_node, Board_Node second_node) {
        return (getAlignedDirection(first_node, second_node) != null);
    }

    Direction getAlignedDirection(Integer first_node_index, Integer second_node_index) {
        Board_Node first_node = findNodeInMap(first_node_index);
        Board_Node second_node = findNodeInMap(second_node_index);

        if (first_node != null && second_node != null)
            return getAlignedDirection(first_node, second_node);
        else return Direction.NULL;
    }

    Direction getAlignedDirection(Board_Node first_node, Board_Node second_node) {
        if (first_node.hasNeighbor(second_node.getId()))
            return first_node.getNeighborDirection(second_node.getId());
        else {
            ArrayList<Integer> adjacent_nodes = first_node.getNeighborsIndexes();

            for (Integer neighbor_index : adjacent_nodes) {
                Direction neighbor_direction = first_node.getNeighborDirection(neighbor_index);
                ArrayList<Integer> aligned_nodes = getAlignedNodesInDirection(neighbor_index, neighbor_direction, new ArrayList<Integer>());

                if (aligned_nodes.contains(second_node.getId()))
                    return neighbor_direction;
            }

            return Direction.NULL;
        }
    }

    private ArrayList<Integer> getAlignedNodesInDirection(Integer current_node_index, Direction direction, ArrayList<Integer> nodes_already_listed) {
        Board_Node current_node = findNodeInMap(current_node_index);

        if (current_node != null)
            return getAlignedNodesInDirection(current_node, direction, nodes_already_listed);
        else return nodes_already_listed;
    }

    private ArrayList<Integer> getAlignedNodesInDirection(Board_Node current_node, Direction direction, ArrayList<Integer> nodes_already_listed) {
        Integer next_node_index = current_node.getIndexOfNeighborInDirection(direction);

        if (next_node_index == -1)
            return nodes_already_listed;
        else {
            nodes_already_listed.add(next_node_index);
            return getAlignedNodesInDirection(next_node_index, direction, nodes_already_listed);
        }
    }
}
