package com.company.Levels;

import com.company.Typedefs.*;

public class Neighbor {
    private Integer id;
    private Direction direction;

    Neighbor (Integer id, Direction direction) {
        this.id = id;
        this.direction = direction;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
