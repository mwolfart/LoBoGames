package com.company.Levels;

public class Piece {
    private Integer id_player;
    private Integer id;
    private Integer board_position;

    public Piece(Integer id_player, Integer id, Integer board_position) {
        this.id_player = id_player;
        this.id = id;
        this.board_position = board_position;
    }

    Integer getPlayer() {
        return id_player;
    }

    Integer getIndex() {
        return id;
    }

    Integer getBoardPosition() {
        return board_position;
    }

    void setBoardPosition(Integer board_position) {
        this.board_position = board_position;
    }

    boolean equalsOwnerAndId(Integer id_player, Integer id_piece) {
        return (this.id_player.equals(id_player) && id.equals(id_piece));
    }
}
