package com.company.Levels;

import com.company.Typedefs.*;

import java.util.ArrayList;

public class Game_Logic {
    private Rules game_rules;
    private ArrayList<Player> player_list;
    private Integer current_turn_owner;

    public Game_Logic(Rules game_rules, ArrayList<Player> player_list) {
        this.game_rules = game_rules;
        this.player_list = player_list;
        current_turn_owner = player_list.get(0).getId();
    }

    public Rules getGameRules() {
        return game_rules;
    }

    public Integer getCurrentTurnOwner() {
        return current_turn_owner;
    }

    public void switchTurn() {
        for (Player player : player_list) {
            if (current_turn_owner.equals(player.getId())) {
                current_turn_owner = player_list.indexOf(player) + 1;
                break;
            }
        }
    }

    public Boolean isMoveValid(Integer id_source, Integer id_target, Board game_board) {
        Integer allowed_move_distance = game_rules.getRuleAllowedMoveDistance();
        ArrayList<Direction> allowed_move_direction = game_rules.getRuleAllowedMoveDirection();
        Integer allowed_capture_distance = game_rules.getRuleAllowedCaptureDistance();
        ArrayList<Direction> allowed_capture_direction = game_rules.getRuleAllowedCaptureDirection();

        if (game_board.isNeighbor(id_source, id_target)) {
            if (!game_board.isNodeFree(id_target))
                return false;
            else return allowed_move_direction.contains(game_board.getAlignedDirection(id_source, id_target));
        }
        else {
            //get distance
            Integer number_of_pieces_on_the_way = game_board.getNumberOfPiecesOnTheWay(id_source, id_target);

            if (!game_board.isAligned(id_source, id_target))
                return false;
            if (number_of_pieces_on_the_way == 1)
                return (allowed_capture_distance > 1 && allowed_capture_direction.contains(game_board.getAlignedDirection(id_source, id_target)));
            else return (number_of_pieces_on_the_way == 0);
        }
    }
}
