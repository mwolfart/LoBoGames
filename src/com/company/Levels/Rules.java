package com.company.Levels;

import com.company.Typedefs.*;

import java.util.ArrayList;

public class Rules {
    //check rules.txt for more info
    private ArrayList<Direction> allowed_move_direction;
    private Integer allowed_move_distance;
    private ArrayList<Direction> allowed_capture_direction;
    private Integer allowed_capture_distance;
    private Integer alignment_trigger;
    private Integer alignment_properties;
    private Integer victory_trigger;
    private Integer victory_piece_limit;

    public void setFlags(ArrayList<Direction> move_adjacent, Integer move_distance, ArrayList<Direction> capture_adjacent, Integer capture_distance, Integer alignment_trigger, Integer alignment_properties, Integer victory_trigger, Integer victory_piece_limit) {
        this.allowed_move_direction = move_adjacent;
        this.allowed_move_distance = move_distance;
        this.allowed_capture_direction = capture_adjacent;
        this.allowed_capture_distance = capture_distance;
        this.alignment_trigger = alignment_trigger;
        this.alignment_properties = alignment_properties;
        this.victory_trigger = victory_trigger;
        this.victory_piece_limit = victory_piece_limit;
    }

    public ArrayList<Direction> getRuleAllowedMoveDirection() {
        return allowed_move_direction;
    }

    public Integer getRuleAllowedMoveDistance() {
        return allowed_move_distance;
    }

    public ArrayList<Direction> getRuleAllowedCaptureDirection() {
        return allowed_capture_direction;
    }

    public Integer getRuleAllowedCaptureDistance() {
        return allowed_capture_distance;
    }

    public Integer getRuleAlignmentTrigger() {
        return alignment_trigger;
    }

    public Integer getRuleAlignmentProperties() {
        return alignment_properties;
    }

    public Integer getRuleVictoryTrigger() {
        return victory_trigger;
    }

    public Integer getRuleVictoryPieceLimit() {
        return victory_piece_limit;
    }
}
