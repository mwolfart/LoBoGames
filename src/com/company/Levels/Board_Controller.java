package com.company.Levels;

public class Board_Controller {
    private Board game_board;
    private Game_Logic game_logic;

    public Board_Controller(Board game_board, Game_Logic logic) {
        this.game_board = game_board;
        this.game_logic = logic;
    }

    public boolean movePiece(Integer id_source, Integer id_target) {
        if (game_logic.isMoveValid(id_source, id_target, game_board)) {
            Piece desired_piece = game_board.getPieceInNode(id_source);
            game_board.setPiecePosition(desired_piece.getPlayer(), desired_piece.getIndex(), id_target);
            return true;
        }
        else return false;
    }
}
