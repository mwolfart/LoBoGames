package com.company.Levels;

import com.company.Typedefs.*;

import java.util.ArrayList;

public class Board_Node {
    private Integer id;
    private ArrayList<Neighbor> neighbors = new ArrayList<Neighbor>();

    public Board_Node(Integer id) {
        this.id = id;
        //Remove piece_id from diagram
        this.neighbors = new ArrayList<Neighbor>(); //Add to diagram
    }

    public void connectNeighbor(Integer id_neighbor, Direction direction) {
        Neighbor new_neighbor = new Neighbor(id_neighbor, direction);
        neighbors.add(new_neighbor);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ArrayList<Neighbor> getNeighbors() {
        return neighbors;
    }

    public void setNeighbors(ArrayList<Neighbor> neighbors) {
        this.neighbors = neighbors;
    }

    private Neighbor findNeighbor(Integer id_neighbor) {
        for (Neighbor current_neighbor : neighbors) {
            if (current_neighbor.getId().equals(id_neighbor))
                return current_neighbor;
        }

        return null;
    }

    boolean hasNeighbor(Integer id_neighbor) {
        Neighbor found_neighbor = findNeighbor(id_neighbor);
        return (found_neighbor != null);
    }

    Direction getNeighborDirection(Integer id_neighbor) {
        Neighbor found_neighbor = findNeighbor(id_neighbor);

        if (found_neighbor == null)
            return Direction.NULL;
        else return found_neighbor.getDirection();
    }
    
    ArrayList<Integer> getNeighborsIndexes() {
        ArrayList<Integer> neighbors_indexes = new ArrayList<Integer>();

        for (Neighbor current_neighbor : neighbors) {
            neighbors_indexes.add(current_neighbor.getId());
        }

        return neighbors_indexes;
    }

    public Integer getIndexOfNeighborInDirection(Direction direction) {
        for (Neighbor current_neighbor : neighbors) {
            if (current_neighbor.getDirection().equals(direction))
                return current_neighbor.getId();
        }

        return -1;
    }
}
