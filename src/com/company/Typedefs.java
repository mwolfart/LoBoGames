package com.company;

public class Typedefs {
    public enum Direction {
        UPLEFT, UP, UPRIGHT, RIGHT, DOWNRIGHT, DOWN, DOWNLEFT, LEFT, FREE, NULL
    }
}